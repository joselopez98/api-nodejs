

const BD = require('../bd.js')
const connection = BD.Conexion();

function initRoute(app){
//Rutas

app.get('/',(req,res) =>{
    res.send('Bienvenidos a mi Api de Peliculas!');
});

//Todas las peliculas 

app.get('/peliculas',(req,res) => {
    const sql = 'SELECT * FROM peliculas';

    connection.query(sql, (error, results) => {
        if (error) throw error;
        if (results.length > 0){
            res.json(results);
        }else{
            res.send('No se Encontraron Peliculas');
        }
    })
});

app.get('/peliculas/:id',(req,res) => {
    const {id} = req.params
    const sql = `SELECT * FROM peliculas WHERE id = ${id}`;

    connection.query(sql, (error, results) => {
        if (error) throw error;
        if (results.length > 0){
            res.json(results);
        }else{
            res.send('No se Encontraron Peliculas');
        }
    })
});

app.post('/add',(req,res)=> {
    const sql = 'INSERT INTO peliculas SET ?';
    
    const peliculaObj ={
        nombre: req.body.nombre,
        tipo: req.body.tipo
    };

    connection.query(sql, peliculaObj, error => {
        if (error) throw error;
        res.send('Pelicula Creada!!');
    });
});

app.put('/update/:id',(req,res)=>{
    const {id} = req.params;
    const {nombre, tipo}  = req.body;

    const sql = `UPDATE peliculas SET nombre = '${nombre}', tipo = '${tipo}' WHERE id = '${id}'`
    
    connection.query(sql,  error => {
        if (error) throw error;
        res.send('Pelicula Actualizada!!');
    });
});

app.delete('/delete/:id',(req,res)=>{
    const {id} = req.params;

    const sql = `DELETE FROM peliculas WHERE id = '${id}'`
    
    connection.query(sql,  error => {
        if (error) throw error;
        res.send('Pelicula Eliminada!!');
    });
});



//verificar conexion

connection.connect(error => {
    if (error) throw error;
    console.log('Database server running!');
});


};

module.exports = {
    initRoute
}

