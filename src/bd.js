const mysql = require('mysql');

function Conexion(){
    const connection = mysql.createConnection({
        host: 'localhost',
        user:'root',
        password:'rootpass',
        database:'node20_mysql'
    });     

    return connection
}

module.exports = {Conexion}

