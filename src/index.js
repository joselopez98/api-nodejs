const express = require('express');
const bodyParser = require('body-parser');
const Pelicula = require('./peliculas/app.js');
const PORT = process.env.PORT || 3050;
const app = express();
app.use(bodyParser.json());
Pelicula.initRoute(app);
app.listen(PORT,() => console.log(`Server running on port ${PORT}`));